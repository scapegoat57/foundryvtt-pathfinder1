_id: 8KEE7ySf9BfSY55O
_key: '!items!8KEE7ySf9BfSY55O'
_stats:
  coreVersion: '12.331'
folder: qf1RdjYT0eba3ceg
img: icons/skills/ranged/cannon-barrel-firing-orange.webp
name: Beam cannon (EMP cannon)
system:
  actions:
    - _id: u2mjm3hlbbakh33p
      ability:
        attack: _default
      actionType: rwak
      activation:
        type: attack
        unchained:
          type: attack
      damage:
        parts:
          - formula: sizeRoll(4, 6, @size)
            types:
              - electric
      duration:
        units: inst
      extraAttacks:
        type: standard
      name: Attack
      notes:
        effect:
          - >-
            A creature that takes damage from a critical hit by an EMP cannon
            must succeed at a DC 15 Fortitude save or be staggered for 1d4
            rounds
      range:
        maxIncrements: 10
        units: ft
        value: '300'
      save:
        harmless: true
      touch: true
      uses:
        perAttack: true
    - _id: 62RRHArEVlPGFNK9
      activation:
        type: full
        unchained:
          cost: 3
          type: action
      area: 20 ft. radius
      damage:
        parts:
          - formula: 8d6
            types:
              - electric
      duration:
        units: inst
      measureTemplate:
        size: '20'
        type: circle
      name: Blast
      notes:
        effect:
          - Androids and creatures with cybernetics take half damage
      range:
        units: ft
        value: '300'
      save:
        dc: '14'
        description: Reflex half
        type: ref
      target:
        value: Robots, Androids or creatures with cybernetics
      uses:
        autoDeductChargesCost: '10'
  baseTypes:
    - Beam cannon (EMP cannon)
  description:
    value: >-
      <h3>Statistics</h3><p><strong>Type</strong> two-handed ranged;
      <strong>Proficiency</strong> exotic (heavy
      weaponry)</p><p><strong>Damage</strong> Special (small), Special (medium);
      <strong>Damage Type</strong> Special; <strong>Critical</strong>
      x2</p><p><strong>Range</strong> 300 ft.; <strong>Capacity</strong> 50;
      <strong>Usage</strong> 1 charge; <strong>Special</strong> Automatic,
      touch</p><h3>Description</h3><p>A beam cannon is an immense version of one
      of the six more common beam rifle types. Beam cannons are generally
      referred to by type as summarized below. In addition to the standard
      automatic firing mode options (<em>Pathfinder Campaign Setting: Technology
      Guide</em> 20), a beam cannon can be used to launch a devastating blast of
      energy. This is a full-round action, and these blasts can't be used to
      make iterative attacks. When a beam cannon is used to launch a blast, no
      attack roll is made-you simply target a 20-foot-radius spread within a
      maximum range of 300 feet. You must have line of effect to your target
      when you launch a blast; if the blast impacts a solid object before
      reaching the target area, it explodes at that point. All creatures caught
      in the blast radius take 6d6 points of damage of the energy type
      appropriate to the cannon (Reflex DC 14 half). Each blast also has an
      additional effect as determined by the type of cannon. Firing a blast from
      a beam cannon consumes 10 charges.</p><p><strong>EMP Cannon</strong>: An
      EMP cannon fires electromagnetic energy that cannot harm most living
      creatures. It deals 4d6 points of electricity damage against robots, and
      half as much damage against androids and creatures with cybernetic
      implants. A creature that takes damage from a critical hit by an EMP
      cannon must succeed at a DC 15 Fortitude save or be staggered for 1d4
      rounds. An EMP cannon's blast deals 8d6 points of electricity damage of
      the same electromagnetic nature. An EMP cannon costs 58,000 gp and has a
      Craft DC of 33.</p><h3>Construction</h3><p><strong>Craft</strong> DC 33;
      <strong>Cost</strong> 29,000
      gp</p><p>@UUID[Compendium.pf1.feats.Item.SkiZsGPMlsMNeBhf]{Craft
      Technological Arms and Armor}, military lab</p>
  hands: 2
  hardness: 10
  hp:
    base: 10
  price: 58000
  properties:
    ato: true
  sources:
    - id: PZO9089
      pages: '62'
  subType: exotic
  uses:
    maxFormula: '50'
    per: charges
    value: 50
  weaponGroups:
    - firearms
  weaponSubtype: ranged
  weight:
    value: 18
type: weapon
