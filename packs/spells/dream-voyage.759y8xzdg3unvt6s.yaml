_id: 759y8xzdg3unvt6s
_key: '!items!759y8xzdg3unvt6s'
_stats:
  coreVersion: '12.331'
folder: Ix6Dc5gXEobiIHKD
img: systems/pf1/icons/misc/magic-swirl.png
name: Dream Voyage
system:
  actions:
    - _id: s3mjx0t4njdv3qm5
      actionType: spellsave
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      duration:
        dismiss: true
        units: hour
        value: '@cl'
      name: Use
      range:
        units: touch
      save:
        description: Will negates
        type: will
      target:
        value: you and one creature/level
  components:
    somatic: true
    verbal: true
  description:
    value: >-
      <p>This spell functions as
      @UUID[Compendium.pf1.spells.Item.laz5zb1cdfb9jzit]{dream travel}, but you
      and your companions travel through the Dimension of Dreams in a
      fantastical vehicle of your own devising that halves the travel time. Only
      you can pilot the vessel, and you can do so even if you aren't inside it.
      Your psychic vehicle buffers minds from the intense emotional tides of the
      Dimension of Dreams, rendering everyone inside immune to harmful emotion
      and fear effects. Unlike with <em>dream travel</em>, there is no chance
      the vessel arrives off target. In addition, you need not have met the
      target creature, but in that case you must have at least a reliable
      description of it. However, if you attempt to travel there with
      insufficient or incorrect information, you do need to roll, using the
      false identity entry from <em>dream travel</em>. </p><p>Creatures that
      disembark from your vehicle can either enter the <em>dreamscape</em> or
      exit to the plane where the dreamer's body lies, arriving within 1 mile of
      the dreamer's body. All creatures that disembark from the vehicle onto a
      plane at the same time arrive in the same place. The dreamer can't prevent
      the <em>dream voyage</em>rs from entering its dream, but can attempt a
      Will save to prevent a group of creatures from exiting to the plane where
      its body resides. </p><p>The psychic vehicle remains in the dream for the
      remainder of the spell's duration, even if it's unoccupied. A creature
      adjacent to the dreamer can reenter the dream vessel as a full-round
      action through an illusory portal only subjects of the spell can see. You
      can return the vessel to the point where you cast <em>dream voyage</em> to
      allow any number of passengers to return to that plane, and can return it
      to the dream again. Either of these trips takes the same amount of time as
      the initial travel, and can be done any number of times within the spell's
      duration. If the vessel moves out of a <em>dreamscape</em>, companions
      left behind can't return to the vessel by being near the dreamer. If a
      companion is killed, its body doesn't return to the <em>dream voyage</em>
      unless you carry it with you, just like any other object. </p><p>If the
      dreamer awakens while the dream vessel is still in its dream, it can allow
      the <em>dream travel</em>ers to exit or not (as with <em>dream
      travel</em>). The dream vessel remains intact in the Dimension of Dreams
      for the spell's duration even if it's untethered from a dream. It drifts
      through <em>dreams</em> when this happens, and you can't pilot it to
      another dream (even a dream of the initial dreamer if that creature begins
      dreaming again). If you dismiss the spell at any time, you and all
      subjects within the dream vessel return to where you started on the
      Material Plane. </p><p>You can increase the number of companions you can
      bring on a <em>dream voyage</em> to 10 creatures per caster level, though
      to do so you must increase the casting time, maintaining your
      concentration and touching up to 6 creatures per round as a full-round
      action. If your concentration is disrupted before you touch all of the
      targets, the spell is wasted and has no effect. Once you have touched all
      of your companions, the <em>dream voyage</em> begins; however, its
      duration is reduced to 1 hour.</p>
  descriptors:
    - mindAffecting
  learnedAt:
    class:
      psychic: 9
  level: 9
  school: con
  sources:
    - id: PZO1132
      pages: '167'
  subschool:
    - teleportation
type: spell
