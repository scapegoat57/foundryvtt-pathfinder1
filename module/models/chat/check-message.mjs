import { BaseMessageModel } from "./base-message.mjs";

/**
 * Data Model for basic check cards.
 */
export class CheckMessageModel extends BaseMessageModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      ...super.defineSchema(),
      dc: new fields.NumberField({ initial: undefined, nullable: false, integer: true }),
    };
  }

  static pruneData(data) {
    super.pruneData(data);

    if (typeof data.dc !== "number") delete data.dc;
  }
}
