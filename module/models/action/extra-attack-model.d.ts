export {};

declare module "./extra-attack-model.mjs" {
  interface ExtraAttackModel {
    /**
     * Attack Name
     */
    name?: string;
    /**
     * Attack Bonus Formula
     */
    formula?: string;
  }
}
