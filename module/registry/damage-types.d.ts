export {};

declare module "./damage-types.mjs" {
  interface DamageType extends RegistryEntry {
    /**
     * Abbreviation
     */
    abbr: string;
    /**
     * Icon
     *
     * Not used if image is defined
     */
    icon?: string;
    /**
     * Image
     *
     * Required if no icon is defined.
     */
    img?: string;
    /**
     * Category
     *
     * @defaultValue `misc`
     */
    category: keyof typeof DamageTypes.CATEGORIES;
    /**
     * Is not real damage type on its own
     */
    isModifier: boolean;
    /**
     * Color
     */
    color: string;
  }
}
