/**
 * Journal text page sheet that displays parentage with sub-pages
 */
export class JournalTextPageSheetPF1 extends JournalTextPageSheet {
  get template() {
    if (this.document.type !== "text" || this.isEditable) return super.template;
    return "systems/pf1/templates/journals/text-view.hbs";
  }

  async getData() {
    const context = await super.getData();

    // Find parent pages if deeper than level 1 heading
    const current = this.document;
    let maxLevel = current.title.level - 1;
    if (maxLevel > 0 && current.parent instanceof JournalEntry) {
      /** @type {JournalEntryPage[]} */
      const pages = current.parent.pages.contents.sort((a, b) => a.sort - b.sort);

      const idx = pages.indexOf(current);
      const headers = [];
      // Traverse backwards
      for (let i = idx - 1; i >= 0; i--) {
        const npage = pages[i];
        const nlevel = npage.title.level;
        if (nlevel <= maxLevel) {
          maxLevel = nlevel - 1;
          headers[maxLevel] = npage;
          if (maxLevel < 1) break;
        }
      }

      context.pf1 = { headers };
    }

    return context;
  }
}
